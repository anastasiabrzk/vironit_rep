const usersController=require('../models/User');

exports.getAll=(req,res,next)=>{
    usersController.find()
        .exec()
        .then(docs=>{
            const response={
                count:docs.length,
                users:docs
            }
            console.log(docs);
            if(docs.length>=0){
                res.status(200).json(response);
            }
            else {
                res.status(404).json({
                    message:'The DB is empty!'
                })
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            })
        })
};
exports.add= (req, res, next) => {
    const league=new usersController({
        firstName:req.body.firstName,
        lastName:req.body.lastName
    });
    league
        .save()
        .then(result=>{
            console.log(result);
            res.status(201).json({
                message: 'Handling POST requests to /users',
                createdLeague: result});

        })
        .catch(err=>{console.log(err);
            res.status(500).json({err:err});
        });

};
exports.update=(req, res, next) => {
    const id = req.params.userId;
    const updateOps={};
    for(const ops of req.body)
        updateOps[ops.propName]=ops.value;
    usersController.update({_id:id},{$set:updateOps})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};
exports.delete =(req, res, next) => {
    const id = req.params.userId;
    usersController.remove({_id:id})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            });
        });

};