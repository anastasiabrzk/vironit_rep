const stageController=require('../models/Stage');

exports.getAll=(req,res,next)=>{
    stageController.find()
        .exec()
        .then(docs=>{
            const response={
                count:docs.length,
                stages:docs
            }
            console.log(docs);
            if(docs.length>=0){
                res.status(200).json(response);
            }
            else {
                res.status(404).json({
                    message:'The DB is empty!'
                })
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            })
        })
};
exports.add= (req, res, next) => {
    const stage=new stageController({
        stageName: req.body.stageName,
        leagueId:req.body.leagueId
    });
    stage
        .save()
        .then(result=>{
            console.log(result);
            res.status(201).json({
                message: 'Handling POST requests to /stage',
                createdStage: result});

        })
        .catch(err=>{console.log(err);
            res.status(500).json({err:err});
        });

};
exports.update=(req, res, next) => {
    const id = req.params.stageId;
    const updateOps={};
    for(const ops of req.body)
        updateOps[ops.propName]=ops.value;
    stageController.update({_id:id},{$set:updateOps})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};
exports.delete =(req, res, next) => {
    const id = req.params.stageId;
    stageController.remove({_id:id})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            });
        });

};

