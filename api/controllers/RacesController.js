const raceController=require('../models/Race');

exports.getAll=(req,res,next)=>{
    raceController.find()
        .exec()
        .then(docs=>{
            const response={
                count:docs.length,
                races:docs
            }
            console.log(docs);
            if(docs.length>=0){
                res.status(200).json(response);
            }
            else {
                res.status(404).json({
                    message:'The DB is empty!'
                })
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            })
        })
};
exports.add= (req, res, next) => {
    const races=new raceController({
        title: req.body.title,
        stageId:req.body.stageId
    });
    races
        .save()
        .then(result=>{
            console.log(result);
            res.status(201).json({
                message: 'Handling POST requests to /races',
                createdRace: result});

        })
        .catch(err=>{console.log(err);
            res.status(500).json({err:err});
        });

};
exports.update=(req, res, next) => {
    const id = req.params.raceId;
    const updateOps={};
    for(const ops of req.body)
        updateOps[ops.propName]=ops.value;
    raceController.update({_id:id},{$set:updateOps})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};
exports.delete =(req, res, next) => {
    const id = req.params.raceId;
    raceController.remove({_id:id})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            });
        });

};