const seasonsController=require('../models/Season');
const leagueController=require('../models/League');
const stageController=require('../models/Stage');
const raceController=require('../models/Race');

//const leagController=require('../models/League');
const mongoose=require('mongoose');
exports.getAll=(req,res,next)=>{
    seasonsController.find()
        .select('idSeason, title')
        .exec()
        .then(docs=>{
            const response={
                count:docs.length,
                Season:docs
            }
            console.log(docs);
            if(docs.length>=0){
                res.status(200).json(response);
            }
            else {
                res.status(404).json({
                    message:'The DB is empty!'
                })
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            })
        })
};
exports.add= (req, res, next) => {
        const Season=new seasonsController({
            title: req.body.title
        });
        Season
            .save()
            .then(result=>{
                console.log(result);
                res.status(201).json({
                    message: 'Handling POST requests to /seasons',
                    createdSeason: result});

            })
            .catch(err=>{console.log(err);
                res.status(500).json({err:err});
            });

    };
exports.update=(req, res, next) => {
    const id = req.params.seasonId;
    const updateOps={};
    for(const ops of req.body)
        updateOps[ops.propName]=ops.value;
    seasonsController.update({_id:id},{$set:updateOps})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};
exports.delete =(req, res, next) => {
    const id = req.params.seasonId;
    seasonsController.remove({_id:id})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            });
        });

};
exports.seeSeasonsWithLeagues=(req,res,next)=>{

    seasonsController.aggregate([

        {
            $lookup:
                {
                    from: "races",
                    localField: "_id",
                    foreignField: "seasonId",
                    as: "league"
                }
        }/*,
        {

            $match:{'_id':{'$eq': mongoose.Types.ObjectId('5c914ed78011613b1cd3b0f0')}}
        }*/

    ])

        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            });
    });
};
exports.findAll=(req,res,next)=>{
    let data= seasonsController.aggregate([
        {
            $match: {
                "seasonId": {$eq:mongoose.Types.ObjectId("5c914ed78011613b1cd3b0f0")}
            }
        },
        {
            $lookup: {
                localField: "_id",
                from: "leagues",
                foreignField: "seasonId",
                as: "League"
            }
        },
        {
            $unwind: {
                path: "$League",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "$League._id",
                from: "stages",
                foreignField: "leagueId",
                as: "$Stage"
            }
        },
        {
            $unwind: {
                path: "$Stage",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "$Stage._id",
                from: "races",
                foreignField: "stageId",
                as: "$Race"
            }
        },
        {
            $unwind: {
                path: "$Race",
                preserveNullAndEmptyArrays: true
            }
        },
         {
             $group: {
                 "_id": "$_id",
                 "Season": {
                     $addToSet: "$_id"
                 },
                 "League": {
                     $addToSet: "$League._id"
                 },
                 "Stage": {
                     $addToSet: "$Stage._id"
                 },
                 "Race": {
                     $addToSet: "$Race._id"
                 }
             }
         }
    ]);
    res.status(200).json({
        message:data
    });
    /*seasonsController.remove({_id:{$in:data[0].Season}});
    leagueController.remove({_id:{$in:data[0].League}});
    stageController.remove({_id:{$in:data[0].Stage}});
    raceController.remove({_id:{$in:data[0].Race}});*/
};