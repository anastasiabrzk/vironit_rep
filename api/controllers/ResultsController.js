const resultsController=require('../models/Result');
exports.getAll=(req,res,next)=>{
    resultsController.find()
        .exec()
        .then(docs=>{
            const response={
                count:docs.length,
                results:docs
            }
            console.log(docs);
            if(docs.length>=0){
                res.status(200).json(response);
            }
            else {
                res.status(404).json({
                    message:'The DB is empty!'
                })
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            })
        })
};
exports.add= (req, res, next) => {
    const results=new resultsController({
        teamId:req.body.teamId,
        raceId:req.body.raceId,
        score:req.body.score
    });
    results
        .save()
        .then(result=>{
            console.log(result);
            res.status(201).json({
                message: 'Handling POST requests to /results',
                createdSeason: result});

        })
        .catch(err=>{console.log(err);
            res.status(500).json({err:err});
        });

};
exports.update=(req, res, next) => {
    const id = req.params.resultId;
    const updateOps={};
    for(const ops of req.body)
        updateOps[ops.propName]=ops.value;
    resultsController.update({_id:id},{$set:updateOps})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};
exports.delete =(req, res, next) => {
    const id = req.params.resultId;
    resultsController.remove({_id:id})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                err:err
            });
        });

};