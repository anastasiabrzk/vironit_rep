const mongoose=require('mongoose');
const{Schema}=mongoose;
//const seasonSchema=require('./Season');

const leagueSchema=new Schema({
    leagueName:String,
    seasonId:Schema.Types.ObjectId
});
module.exports=mongoose.model('League',leagueSchema);