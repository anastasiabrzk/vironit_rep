const mongoose=require('mongoose');
const{Schema}=mongoose;

const raceSchema=new Schema({
    stageId:Schema.Types.ObjectId,
    title:String
});
module.exports=mongoose.model('Race',raceSchema);