const mongoose=require('mongoose');
const{Schema}=mongoose;

const stageSchema=new Schema({
    leagueId:Schema.Types.ObjectId,
    stageName:String
});
module.exports=mongoose.model('Stage',stageSchema);