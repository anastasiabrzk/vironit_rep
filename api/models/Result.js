const mongoose=require('mongoose');
const{Schema}=mongoose;

const resultSchema=new Schema({
    teamId:Schema.Types.ObjectId,
    raceId:Schema.Types.ObjectId,
    resultId:Schema.Types.ObjectId,
    score:Number
});
module.exports=mongoose.model('Result',resultSchema);