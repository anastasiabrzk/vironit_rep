const mongoose=require('mongoose');
const{Schema}=mongoose;

const teamSchema=new Schema({
    userId : Schema.Types.ObjectId,
    teamName : String
});
module.exports=mongoose.model('Team',teamSchema);