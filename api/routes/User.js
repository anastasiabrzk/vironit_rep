const express = require('express');
const router = express.Router();

const userController=require('../controllers/UsersController');


router.get('/',userController.getAll);
router.post('/', userController.add);
router.patch('/:userId',userController.update);
router.delete('/:userId',userController.delete);
//router.get('/seasonsWithLeagues',userController.seeSeasonsWithLeagues);
module.exports=router;
