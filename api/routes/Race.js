const express = require('express');
const router = express.Router();


const raceController=require('../controllers/RacesController');


router.get('/',raceController.getAll);
router.post('/', raceController.add);
router.patch('/:raceId',raceController.update);
router.delete('/:raceId',raceController.delete);
module.exports=router;
