const express = require('express');
const router = express.Router();

const seasonController=require('../controllers/SeasonsController');


router.get('/',seasonController.getAll);
router.post('/', seasonController.add);
router.patch('/:seasonId',seasonController.update);
router.delete('/:seasonId',seasonController.delete);
router.get('/seasonsWithLeagues',seasonController.seeSeasonsWithLeagues);
router.delete('/findAll',seasonController.findAll);
module.exports=router;
