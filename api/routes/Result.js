const express = require('express');
const router = express.Router();


const resultController=require('../controllers/ResultsController');


router.get('/',resultController.getAll);
router.post('/', resultController.add);
router.patch('/:resultId',resultController.update);
router.delete('/:resultId',resultController.delete);
module.exports=router;
