const express = require('express');
const router = express.Router();


const leaguesController=require('../controllers/LeaguesController');

//const seasonModel=require('../models/Season');
//const leagueModel=require('../models/League');


router.get('/',leaguesController.getAll);
router.post('/', leaguesController.add);
router.patch('/:leagueId',leaguesController.update);
router.delete('/:leagueId',leaguesController.delete);
module.exports=router;
