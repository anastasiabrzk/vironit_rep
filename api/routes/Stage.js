const express = require('express');
const router = express.Router();

const stageController=require('../controllers/StagesController');


router.get('/',stageController.getAll);
router.post('/', stageController.add);
router.patch('/:stageId',stageController.update);
router.delete('/:stageId',stageController.delete);
//router.get('/all',stageController.findAll);
module.exports=router;
