const express = require('express');
const router = express.Router();

const teamController=require('../controllers/TeamsController');


router.get('/',teamController.getAll);
router.post('/', teamController.add);
router.patch('/:teamId',teamController.update);
router.delete('/:teamId',teamController.delete);
//router.get('/seasonsWithLeagues',teamController.seeSeasonsWithLeagues);
module.exports=router;
