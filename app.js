const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose=require('mongoose');

const seasonRoutes = require("./api/routes/Season");
const leagueRoutes = require("./api/routes/League");
const stageRoutes = require("./api/routes/Stage");
const raceRouter =require("./api/routes/Race");
const userRouter=require("./api/routes/User");
const teamRouter=require("./api/routes/Team");
const resultRouter=require("./api/routes/Result");

//const authorisationRoutes = require("./api/routes/authorisation");

mongoose.connect("mongodb://localhost:27017/races", { useNewUrlParser: true }, function(err){
    if(err) return console.log(err);
    else console.log('Connected to DB');
});
app.use(express.static(__dirname + "/front"));
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

// Routes which should handle requests

app.use("/", seasonRoutes);
app.use("/league", leagueRoutes);
app.use("/stage", stageRoutes);
app.use("/race",raceRouter);
app.use("/users", userRouter);
app.use("/team", teamRouter);
app.use("/result", resultRouter);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;